%define SYS_WRITE 1
%define FD_STDERR 2 
%define LEN_BUF 255
%define LEN_ERR_LONG 62
%define LEN_ERR_NOT 50

%include "dict.inc"
;extern find_word
%include "words.inc"
%include "lib.inc"
	
global _start

section .rodata
error_not_found:
	db "ERROR: This word don't contained in the dictionary", 0
error_long_str:
	db "ERROR: This string is so long for buffer, try to write shorter", 0
;section .data
;our_word:
;	db 255,  ""


section .bss
our_word: resb 256





section .text


_start:
	;читает строку длинной не более 255 символов с stdin
	;пытается найти вхождение в словарь
	;если оно есть печатает значение по ключу
	;иначе печатает сообщение об ошибке
	mov rdi, our_word
	mov rsi, LEN_BUF
	call read_word 
	;rax - хранится адрес буфера
	test rax, rax
	je .end_long_word
	;Переходим к поиску слова в словаре
	
	.find_word_in:
		mov rdi, rax
		mov rsi, first_word  ;указатель на словарь на первый элемент в словаре
		call find_word	
		test rax, rax
		je .end_not_found
		
		mov rdi, rax
		call print_string
		call exit

	.end_long_word:
		mov rax, SYS_WRITE
		mov rdi, FD_STDERR
		mov rdx, LEN_ERR_LONG
		mov rsi, error_long_str
		syscall
		call exit
	.end_not_found:
		mov rax, SYS_WRITE
		mov rdi, FD_STDERR
		mov rdx, LEN_ERR_NOT
		mov rsi, error_not_found
		syscall
		call exit
