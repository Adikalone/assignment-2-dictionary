ASM=nasm
ASMFLAGS=-felf64 -o
.PHONY: clean test

test:
	python3 test.py

clean:
	rm *.o product


lib.o: lib.asm
	$(ASM) $(ASMFLAGS) lib.o lib.asm	
dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) dict.o dict.asm
main.o: dict.o lib.o dict.inc words.inc
	$(ASM) $(ASMFLAGS) main.o main.asm

product: main.o dict.o lib.o
	ld -o product $^
