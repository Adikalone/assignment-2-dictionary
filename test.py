import subprocess
import unittest


class TestDictionary(unittest.TestCase):
    def run_product(self, input):
        process = subprocess.Popen(["./product"], shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE, stdin=subprocess.PIPE, text=True)
        stdout, stderr = process.communicate(input=input)
        return (stdout.strip(), stderr.strip())

    def test_first(self):
        self.assertEqual(self.run_product("first"), ("first word explanation", ""))

    def test_second(self):
        self.assertEqual(self.run_product("second"), ("second word explanation", ""))

    def test_third(self):
        self.assertEqual(self.run_product("third"), ("third word explanation", ""))

    def test_long_str(self):
        self.assertEqual(self.run_product("H"*257), ("", "ERROR: This string is so long for buffer, try to write shorter"))

    def test_not_found(self):
        self.assertEqual(self.run_product("Hello there! General Kenobi"), ("", "ERROR: This word don't contained in the dictionary"))

if __name__ == "__main__":
    unittest.main()
