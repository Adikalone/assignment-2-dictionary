global exit
global string_length
global print_char
global print_string
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global strig_copy

%define SYS_EXIT  60
%define SYS_READ 0
%define SYS_WRITE 1

%define FD_STDOUT 1



section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, SYS_EXIT
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    ; в rdi хранится указатель на нуль-терминированную строку
    xor rax, rax
	.loop:
		cmp byte [rdi + rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	mov rdx, rax 
	pop rsi
	mov rax, SYS_WRITE
	mov rdi, FD_STDOUT
	syscall
	ret







; Принимает код символа и выводит его в stdout
print_char:
    push rdi
	mov rsi, rsp
	mov rax, SYS_WRITE
	mov rdx, FD_STDOUT
    mov rdi, 1 ; len of the one char
	syscall
    pop rdi
	ret
    
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	push 0xA
	
	mov rsi, rsp
	mov rax, SYS_WRITE
	mov rdx, FD_STDOUT
	mov rdi, 1 ; len of the one char
	syscall
	
	pop rdi
	
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        ; rdi - хранит наше число
        push rbx
        mov rax, rdi ; rax - само наше число, которое мы будем делить

        xor rdx, rdx

        mov rdi, 0xa ; основание системы счисления
        mov rbx, rsp ; rbx - адрес нашей строки (числа)
        sub rbx, 1024
	
    	xor r8, r8
        mov [rbx], r8b; push 0
        sub rbx, 1
        .loop:
                xor rdx, rdx
                div rdi
                mov rcx, rdx
                add rdx, 48 ; превращаем число в символ
                mov [rbx], dl
                sub rbx, 1
                test rax, rax
                jne .loop
        .print:
                add rbx, 1
                mov rdi, rbx
                call print_string
        .end:
                pop rbx
                ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
        ; rdi - приходит значение
        test rdi, rdi
        jge .plus


        neg rdi
        mov rax, rdi
        mov rdi, '-' ; '-' = 0x2d
        push rax
        call print_char
        pop rax
        mov rdi, rax
       ; pop rax
        call print_uint
        ret
        .plus:
                call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	; rdi - указатель на первую строку
        ; rsi - указатель на вторую строку
	mov r8, rdi
	mov r9, rsi
;        push r10 ; будет длинной первой строки
;        push r11 ; будет длинной второй строки
	; r10 - first str len
	; r11 - second str len
	
	.strlenfirst:
		mov rdi, r8
		push r8
		push r9
		
		call string_length
	
		pop r9
		pop r8
		
		mov r10, rax
	.strlensecond:
		mov rdi, r9

		push r8
		push r9
		push r10
	
		call string_length
	
		pop r10
		pop r9
		pop r8
	
		mov r11, rax
	.comparelen:
		cmp r10, r11
		jne .with0
		
		cmp r10, 0
		je .with1

		xor rcx, rcx
	
        .loop:
                mov dil, byte [r8 + rcx]
                mov sil, byte [r9 + rcx]
                cmp rdi, rsi
                jne .with0
		inc rcx
                cmp rcx, r10
                je .with1
                jmp .loop


        .end:
                ret
        .with1:
                mov rax, 1
		jmp .end
	.with0:
		mov rax, 0
		jmp .end







; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	
	
	push r13

        mov r13, rsp
        sub r13, 0x80
        mov byte[r13], 0


        mov rax, 0 ; num read_sys
        mov rdi, 0 ; stdin fd
        mov rsi, r13 ; buf
        mov rdx, 1 ; size


        syscall ; read syscal
        cmp rax, 0
        je .end

        mov al, byte [r13]

        .end:
                pop r13
                ret
	
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	; 0x20 - 
	; 0x9 - skip in start, end in the end
	; 0xA -
	; rdi - адрес начала буффера
	; rsi - длина буфера
	
;save section
	
	mov r10, rdi ; адрес
	mov r11, rsi ; длина
	
	xor rcx, rcx ; счетчик



	.readSymbol:
		push r10
		push r11
		push rcx
		call read_char
		; rax = our char

		pop rcx
		pop r11
		pop r10
	.Space:
		cmp rax, 0x9
		je .SpaceCondition
		cmp rax, 0xA
		je .SpaceCondition
		cmp rax, 0x20
		je .SpaceCondition
		cmp rax, 0x0
		je .end
		jmp .checkConditions
	.SpaceCondition:	
		cmp rcx, 0
		je .readSymbol
		jmp .end 
	.checkConditions:
		cmp r11, rcx
		je .endwithproblem
		; end of len of the buffer	
	.writeSymbol:
		mov byte [r10 + rcx], al
		inc rcx
		jmp .readSymbol
	.end:
		mov byte [r10 + rcx], 0
		mov rax, r10
		mov rdx, rcx
		ret	
	
	.endwithproblem:	
		mov rax, 0
		ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	; принимает указатель на строку
	; rdi - указатель на строку
	
	; коды цифр 
	; 0 = 0х30 = 48
	; 9 = 0х39 = 57


	; буду считывать цифру. Если есть ещё одна, домножаем на 10 и прибавляем её, поворить.
	; можно читать и класть нужные символы на стек. Определяем длину нашего числа и возвращаемся
;start position
	
	xor rcx, rcx ; счетчик цифр
	mov r8, rdi  ; указатель на строку
	xor rdx, rdx ; текущая цифра
	
	.getchar:
		mov dl, byte [r8 +rcx]
	.Conditionforend:
		cmp rdx, 0x30
		jb .beforemath
		cmp rdx, 0x39
		ja .beforemath
	.putonsteck:
		sub rdx, 0x30
		push rdx
		inc rcx
		jmp .getchar
	
	.beforemath:
		mov r9, rcx
		xor rcx, rcx
		xor rsi, rsi ; наше будующее число
		mov rax, 1 ; будем наращивать 10 здесь
		xor r10, r10 ; будет цифрой, которую мы снимаем со стека
		
		; r9 - длина нашей цифры
	.math:
		cmp r9, 0
		je .endwithproblem
		; текущее * 10^(ткущая длина) + результат_предыдущего
		inc rcx
		pop r10
		push rax
		mul r10
		xor rdx, rdx
		add rsi, rax
		pop rax 

		cmp r9, rcx
		je .end

	
		; в rax находится степень 10
		mov r10, 10
		mul r10
		xor rdx, rdx
		jmp .math
	.end:
		mov rax, rsi
		mov rdx, r9
		ret
	
	.endwithproblem:
		mov rdx, 0
		mov rax, 0
		ret
		




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	; rdi - string pointer
	mov r8, rdi
	.getfirst:
		xor rsi, rsi
		mov sil, byte [r8]
		cmp rsi, 0x2d
		je .sign
	.nosign:
		mov rdi, r8
		call parse_uint
		jmp .end
	.sign:
		inc r8
		mov rdi, r8
		call parse_uint
        test rdx, rdx
		je .end
		push rdx
		mov r10, -1
		imul r10
		pop rdx
		inc rdx
	.end:
		ret

	

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	; rdi - string pointer
	; rsi - buffer pointer
	; rdx - buffer length

	mov r8, rdi
	mov r9, rsi
	mov r10, rdx
	
	xor rsi, rsi ; 
	xor rcx, rcx
	.readchar:
		mov sil, byte [r8 + rcx]
	.conditionlenbuf:
		test rsi, rsi
		je .end 
		;
		cmp r10, rcx
		je .endwithproblem
	.writechar:
		mov byte [r9 + rcx], sil
		inc rcx
		jmp .readchar
	.end:
		mov byte [r9 + rcx], 0
		mov rax, rcx
		ret
	.endwithproblem:
		mov rax, 0
		ret


