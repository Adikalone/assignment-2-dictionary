global find_word
%include "lib.inc"
section .text

;Принимает:
;указатель на 0-терменированую строку - rdi
;указатель на начало словаря - rsi
;Проходить по всему словарю в поисках подходящего ключа
;Возвращает:
;Если найдено подходящее значение, то вернёт адрес начала вхождения в словарь
;Иначе вернёт 0
find_word:
	.loop:
	mov r8, rsi ; Сохраняем в r8 указатель на следующий элемент словаря
	add rsi, 8 ; Записываем в rsi адрес начала ключа словаря
	push r8
	push rdi
	push rsi
	call string_equals ; Вызываем функцию сравнения двух строк
	pop rsi
	pop rdi
	pop r8
	test rax, rax ; Если строки равны, то будет 1, иначе 0
	je .nextword ; В случае 0, надо взять следующее слово
	
	.ourword:
		push rsi
		mov rdi, rsi
		call string_length
		pop rsi
		add rsi, rax
		inc rsi
		mov rax, rsi
		ret
	;mov rax, rsi
	;ret


	.nextword:
		mov r9, qword [r8]
		test r9, r9 ; Проверяем, есть ли следующее слово
		je .badend
		mov rsi, qword [r8] ; Получаем адрес указатель на метку второго слова
		jmp .loop ; Проворачиваем сравнение занов
	

	.badend:
		mov rax, 0
		ret
	
	
	


